import { Controller } from '@hotwired/stimulus';

import {createRoot} from 'react-dom/client';
import React from 'react';
import { ThirdwebProvider,ChainId, ConnectWallet } from "@thirdweb-dev/react";


/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="hello" attribute will cause
 * this controller to be executed. The name "hello" comes from the filename:
 * hello_controller.js -> "hello"
 *
 * Delete this file or adapt it for your use!
 */
export default class extends Controller {
    connect() {
        createRoot(this.element).render(
            <ThirdwebProvider desiredChainId={ChainId.Goerli}>
                <ConnectWallet/>
            </ThirdwebProvider>
        )
    }

/*     async test() {
          const sdk = new ThirdwebSDK("mumbai");
          const contract = await sdk.getContract("0x5aef0e4fa95017d6dd4A73d06097E87a5C5a3c38", "nft-collection");
          console.log(await contract.getAll());
     }*/
}
